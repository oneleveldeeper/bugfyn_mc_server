#Carpenter's Daylight Sensor
recipes.remove(<CarpentersBlocks:blockCarpentersDaylightSensor>);

#Carpenter's Safe
recipes.remove(<CarpentersBlocks:blockCarpentersSafe>);
recipes.addShaped(<CarpentersBlocks:blockCarpentersSafe>,
    [
        [<CarpentersBlocks:blockCarpentersBlock>, <CarpentersBlocks:blockCarpentersBlock>, <CarpentersBlocks:blockCarpentersBlock>],
        [<CarpentersBlocks:blockCarpentersBlock>, <terrafirmacraft:item.Wrought Iron Double Sheet>, <CarpentersBlocks:blockCarpentersBlock>],
        [<CarpentersBlocks:blockCarpentersBlock>, <minecraft:redstone>, <CarpentersBlocks:blockCarpentersBlock>]
    ]);

#Carpenter's Torch
recipes.remove(<CarpentersBlocks:blockCarpentersTorch>);

#Carpenter's Hammer
recipes.remove(<CarpentersBlocks:itemCarpentersHammer>);
recipes.addShaped(<CarpentersBlocks:itemCarpentersHammer>,
    [
        [<terrafirmacraft:item.Wrought Iron Ingot>, <terrafirmacraft:item.Wrought Iron Ingot>, null],
        [null, <CarpentersBlocks:blockCarpentersBlock>, <terrafirmacraft:item.Wrought Iron Ingot>],
        [null, <CarpentersBlocks:blockCarpentersBlock>, null]
    ]);

#Carpenter's Chisel
recipes.remove(<CarpentersBlocks:itemCarpentersChisel>);
recipes.addShaped(<CarpentersBlocks:itemCarpentersChisel>,
    [
        [<terrafirmacraft:item.Wrought Iron Ingot>],
        [<CarpentersBlocks:blockCarpentersBlock>]
    ]);

#Carpenter's Tile
recipes.remove(<CarpentersBlocks:itemCarpentersTile>);
recipes.addShaped(<CarpentersBlocks:itemCarpentersTile>,
    [
        [<terrafirmacraft:item.Clay>, <terrafirmacraft:item.Clay>, <terrafirmacraft:item.Clay>],
        [<CarpentersBlocks:blockCarpentersBlock>, <CarpentersBlocks:blockCarpentersBlock>, <CarpentersBlocks:blockCarpentersBlock>],
        [<terrafirmacraft:item.Clay>, <terrafirmacraft:item.Clay>, <terrafirmacraft:item.Clay>]
    ]);

#Carpenter's Bed
recipes.remove(<CarpentersBlocks:itemCarpentersBed>);
recipes.addShaped(<CarpentersBlocks:itemCarpentersBed>,
    [
        [<terrafirmacraft:item.WoolCloth>, <terrafirmacraft:item.WoolCloth>, <terrafirmacraft:item.WoolCloth>],
        [<CarpentersBlocks:blockCarpentersBlock>, <CarpentersBlocks:blockCarpentersBlock>, <CarpentersBlocks:blockCarpentersBlock>]
    ]);