#!/bin/bash
MINECRAFT_JAR="forge-1.7.10-10.13.4.1614-1.7.10-universal.jar"
cd ${BASH_SOURCE%/*}
screen -d -m -S mcs java -Xms6G -Xmx6G -d64 -jar $MINECRAFT_JAR nogui
